// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonusBase.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBonusBase::ASpeedBonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedBonusBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedBonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->MovementSpeed *= 1.5f;
		Destroy();
	}
}

