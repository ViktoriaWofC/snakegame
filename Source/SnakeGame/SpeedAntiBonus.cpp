// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedAntiBonus.h"
#include "SnakeBase.h"

// Sets default values
ASpeedAntiBonus::ASpeedAntiBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedAntiBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedAntiBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeedAntiBonus::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->MovementSpeed /= 1.5f;
		Destroy();
	}
}

